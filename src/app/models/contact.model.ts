export interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  birthDay: string;
  email: string;
  address: string;
}
