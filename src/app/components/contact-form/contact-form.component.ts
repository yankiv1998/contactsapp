import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Contact } from 'src/app/models/contact.model';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  contactForm!: FormGroup;
  @Input() isEditing: boolean = false;
  @Input() contact?: Contact;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactsService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      id: [this.contact?.id || ''],
      firstName: [
        this.contact?.firstName || '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      lastName: [
        this.contact?.lastName || '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      phoneNumber: [
        this.contact?.phoneNumber || '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(13),
        ],
      ],
      birthDay: [
        this.contact?.birthDay || '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
        ],
      ],
      email: [
        this.contact?.email || '',
        [Validators.required, Validators.email],
      ],
      address: [this.contact?.address || '', Validators.required],
    });
  }
  public onSubmit() {
    if (this.contactForm.valid) {
      if (this.isEditing) {
        this.contactService.editContact(this.contactForm.value);
        this.contactForm.reset();
        this.router.navigate(['']);
      } else {
        this.contactService.addContact(this.contactForm.value);
        this.contactForm.reset();
        this.router.navigate(['']);
      }
    }
  }
  public cancel() {
    this.router.navigate(['']);
  }
}
