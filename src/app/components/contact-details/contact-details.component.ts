import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from 'src/app/models/contact.model';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss'],
})
export class ContactDetailsComponent implements OnInit {
  public detailsInfo: Contact[] = [];
  public selectedContact: Contact | undefined;
  public isEditing: boolean = false;

  constructor(
    private contactsService: ContactsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.detailsInfo = this.contactsService.getDetails();
  }

  public onEditContact(value: Contact) {
    this.isEditing = true;
    this.selectedContact = value;
  }
  public onHomePage() {
    this.router.navigate(['']);
  }
}
