import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  public searchText: string = '';
  @Output() searchEvent: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
  public onSearch() {
    this.searchEvent.emit(this.searchText);
  }
}
