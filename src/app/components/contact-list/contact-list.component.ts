import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact.model';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
})
export class ContactListComponent implements OnInit {
  public contacts: Contact[] = [];
  public filteredContacts: Contact[] = [];

  constructor(private contactsService: ContactsService) {}

  ngOnInit(): void {
    this.contacts = this.contactsService.getContacts();
    this.filteredContacts = [...this.contacts];
  }
  public removeContact(id: number) {
    this.contactsService.deleteContact(id);
    this.contacts = this.contactsService.getContacts();
    this.filteredContacts = [...this.contacts];
  }
  public onSearch(searchText: string) {
    if (!searchText) {
      this.filteredContacts = [...this.contacts];
      return;
    }
    this.filteredContacts = this.contacts.filter(
      (contact) =>
        contact.firstName
          .toLocaleLowerCase()
          .includes(searchText.toLocaleLowerCase()) ||
        contact.lastName
          .toLocaleLowerCase()
          .includes(searchText.toLocaleLowerCase()),
    );
  }
  public detailsInfo(id: number) {
    this.contactsService.detailedContactInfo(id);
  }
}
