import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';

@Injectable({
  providedIn: 'root',
})
export class ContactsService {
  private contacts: Contact[] = [];
  private localStorageKey = 'contacts';
  private contactDetails: Contact[] = [];
  private localStorageDetailsKey = 'details';

  constructor() {
    const storedContacts = localStorage.getItem(this.localStorageKey);
    const storedDetails = localStorage.getItem(this.localStorageDetailsKey);
    storedDetails ? (this.contactDetails = JSON.parse(storedDetails)) : null;
    if (storedContacts) {
      this.contacts = JSON.parse(storedContacts);
    } else {
      this.contacts = [
        {
          id: 1,
          firstName: 'John',
          lastName: 'Doe',
          phoneNumber: '1234567890',
          birthDay: '1990-01-01',
          email: 'john@example.com',
          address: '123 Main St',
        },
        {
          id: 2,
          firstName: 'Jane',
          lastName: 'Smith',
          phoneNumber: '0987654321',
          birthDay: '1985-05-15',
          email: 'jane@example.com',
          address: '456 Elm St',
        },
        {
          id: 3,
          firstName: 'Alice',
          lastName: 'Johnson',
          phoneNumber: '5556667777',
          birthDay: '1995-12-30',
          email: 'alice@example.com',
          address: '789 Oak St',
        },
      ];
    }
    this.saveContacts();
  }

  public getContacts(): Contact[] {
    return this.contacts;
  }
  public getDetails(): Contact[] {
    return this.contactDetails;
  }
  private saveContacts(): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.contacts));
  }
  public addContact(contact: Contact) {
    contact.id = this.generateId();
    this.contacts.push(contact);
    this.saveContacts();
  }
  public editContact(contact: Contact) {
    const index = this.contacts.findIndex((item) => item.id === contact.id);
    this.contacts[index] = contact;
    this.saveContacts();
  }
  private generateId(): number {
    const lastId =
      this.contacts.length > 0 ? this.contacts[this.contacts.length - 1].id : 0;
    return lastId + 1;
  }
  public deleteContact(id: number) {
    this.contacts = this.contacts.filter((item) => item.id !== id);
    this.saveContacts();
  }
  public detailedContactInfo(id: number) {
    this.contactDetails = this.contacts.filter((item) => item.id === id);
    localStorage.setItem(
      this.localStorageDetailsKey,
      JSON.stringify(this.contactDetails),
    );
  }
}
